<?php

namespace JeniaPlah\Demo\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class FirstOutput
 * @package JeniaPlah\Demo\Block
 */
class HelloWorld extends Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHelloWorld()
    {
        return __('Hello world');
    }
}