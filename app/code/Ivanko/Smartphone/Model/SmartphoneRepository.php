<?php

namespace Ivanko\Smartphone\Model;

use Ivanko\Smartphone\Api\SmartphoneRepositoryInterface;
use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Ivanko\Smartphone\Api\Data\SmartphoneInterfaceFactory;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone as Resource;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class SmartphoneRepository implements SmartphoneRepositoryInterface
{
    /**
     * @var SmartphoneInterfaceFactory
     */
    private $smartphoneFactory;

    /**
     * @var Resource
     */
    private $smartphoneResource;

    /**
     * @var Resource\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * SmartphoneRepository constructor.
     * @param SmartphoneInterfaceFactory $smartphoneFactory
     * @param Resource $smartphoneResource
     * @param Resource\CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        SmartphoneInterfaceFactory $smartphoneFactory,
        Resource $smartphoneResource,
        Resource\CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->smartphoneFactory = $smartphoneFactory;
        $this->smartphoneResource = $smartphoneResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param integer $id
     * @return SmartphoneInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        /** @var SmartphoneInterface $smartphone */
        $smartphone = $this->smartphoneFactory->create();
        $this->smartphoneResource->load($smartphone, $id);
        if (!$smartphone->getId())
            throw new NoSuchEntityException(__("The smartphone with the '%1' ID doesn't exist.", $id));
        return $smartphone;
    }

    /**
     * @param SmartphoneInterface $smartphone
     * @return SmartphoneRepositoryInterface
     * @throws CouldNotSaveException
     */
    public function save($smartphone)
    {
        try {
            $this->smartphoneResource->save($smartphone);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __("Could not save the smartphone: %1", $exception->getMessage()),
                $exception
            );
        }
        return $this;
    }

    /**
     * @param SmartphoneInterface $smartphone
     * @throws \Exception
     */
    public function delete($smartphone)
    {
        try {
            $this->smartphoneResource->delete($smartphone);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                    "Could not delete the smartphone: %1",
                    $exception->getMessage())
            );
        }
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList($searchCriteria)
    {
        /** @var Resource\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        /** @var \Magento\Framework\Api\SearchResultsInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }
}