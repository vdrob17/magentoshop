<?php

namespace Ivanko\Smartphone\Api\Data;

interface SmartphoneInterface
{
    const ID = 'id';
    const MODEL = 'model';
    const DISPLAY = 'display';
    const RESOLUTION = 'resolution';
    const PROCESSOR = 'processor';
    const OS = 'os';
    const STORAGE = 'storage';
    const RAM = 'ram';
    const CAMERA = 'camera';
    const BATTERY = 'battery';
    const PRICE = 'price';
    const DESCRIPTION = 'description';
    const STATUS = 'status';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param integer $id
     * @return SmartphoneInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getModel();

    /**
     * @param string $model
     * @return SmartphoneInterface
     */
    public function setModel($model);

    /**
     * @return string
     */
    public function getDisplay();

    /**
     * @param string $display
     * @return SmartphoneInterface
     */
    public function setDisplay($display);

    /**
     * @return string
     */
    public function getResolution();

    /**
     * @param string $resolution
     * @return SmartphoneInterface
     */
    public function setResolution($resolution);

    /**
     * @return string
     */
    public function getProcessor();

    /**
     * @param string $processor
     * @return SmartphoneInterface
     */
    public function setProcessor($processor);

    /**
     * @return string
     */
    public function getOs();

    /**
     * @param string $os
     * @return SmartphoneInterface
     */
    public function setOs($os);

    /**
     * @return integer
     */
    public function getStorage();

    /**
     * @param integer $storage
     * @return SmartphoneInterface
     */
    public function setStorage($storage);

    /**
     * @return integer
     */
    public function getRam();

    /**
     * @param integer $ram
     * @return SmartphoneInterface
     */
    public function setRam($ram);

    /**
     * @return string
     */
    public function getCamera();

    /**
     * @param string $camera
     * @return SmartphoneInterface
     */
    public function setCamera($camera);

    /**
     * @return integer
     */
    public function getBattery();

    /**
     * @param integer $battery
     * @return SmartphoneInterface
     */
    public function setBattery($battery);

    /**
     * @return float
     */
    public function getPrice();

    /**
     * @param float $price
     * @return SmartphoneInterface
     */
    public function setPrice($price);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return SmartphoneInterface
     */
    public function setDescription($description);

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param integer $status
     * @return SmartphoneInterface
     */
    public function setStatus($status);
}