<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Elogic\Test\Controller\Index;

use Elogic\Link\Api\Data\ErpNumberInterfaceFactory;
use Elogic\Link\Api\ErpNumberRepositoryInterface;
use Elogic\Link\Model\ResourceModel\ErpNumber;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Custom page for storefront. Needs to be accessible by POST because of the store switching.
 */
class Order extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var ProductRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ErpNumberInterfaceFactory $erpNumberFactory
     */
    private $erpNumberFactory;

    /**
     * @var ErpNumberRepositoryInterface $erpNumberRepository
     */
    private $erpNumberRepository;

    /**
     * Index constructor.
     * @param PageFactory $pageFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param ErpNumberRepositoryInterface $erpNumberRepository
     * @param ErpNumberInterfaceFactory $erpNumberInterfaceFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $pageFactory,
        OrderRepositoryInterface $orderRepository,
        ErpNumberRepositoryInterface $erpNumberRepository,
        ErpNumberInterfaceFactory $erpNumberInterfaceFactory,
        Context $context
    ) {
        $this->pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->erpNumberFactory = $erpNumberInterfaceFactory;
        $this->erpNumberRepository = $erpNumberRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
//        //die('1');
//        /**var $product Product */
//        $product = $this->productRepository->getById(1);
//        echo $product->getName();
//        echo $product->getSKU();
//        echo "<br />";
//        echo $product->getPrice();

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get("1");
        $extensionAttributes = $order->getExtensionAttributes();
        $erpAttribute = $extensionAttributes->getErpNumber();
        echo $erpAttribute->getErpId();die;

//        $erpNumber = $this->erpNumberFactory->create();
//        $erpNumber->setErpId(rand(1,100000));
//        $erpNumber->setOrderId($order->getId());
//
//        $this->erpNumberRepository->save($erpNumber);
//
//        $extensionAttributes = $order->getExtensionAttributes();
//        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
//        $orderExtension->setErpNumber($erpNumber);
//        $order->setExtensionAttributes($orderExtension);
//
//        $order = $this->orderRepository->save($order);
        
//       echo  $order->getId();



//        return  $resultPage = $this->pageFactory->create();
    }
}