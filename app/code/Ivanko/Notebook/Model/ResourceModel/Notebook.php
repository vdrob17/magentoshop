<?php

namespace Ivanko\Notebook\Model\ResourceModel;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Notebook extends AbstractDb
{
    const TABLE_NAME = 'notebook';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, NotebookInterface::ID);
    }
}