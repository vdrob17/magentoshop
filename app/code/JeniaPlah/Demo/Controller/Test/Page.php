<?php

namespace JeniaPlah\Demo\Controller\Test;

class Page extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        echo 'Page';
    }
}