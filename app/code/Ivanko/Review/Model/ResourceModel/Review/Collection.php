<?php

namespace Ivanko\Review\Model\ResourceModel\Review;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            \Ivanko\Review\Model\Review::class,
            \Ivanko\Review\Model\ResourceModel\Review::class
        );
    }
}