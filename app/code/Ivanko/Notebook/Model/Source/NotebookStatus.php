<?php

namespace Ivanko\Notebook\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class NotebookStatus implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Disable')],
            ['value' => 1, 'label' => __('Enable')]
        ];
    }
}