<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Elogic\Test\Controller\Index;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

/**
 * Custom page for storefront. Needs to be accessible by POST because of the store switching.
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Index constructor.
     * @param PageFactory $pageFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Context $context
     */
    public function __construct(
        PageFactory $pageFactory,
        ProductRepositoryInterface $productRepository,
        Context $context
    ) {
        $this->pageFactory = $pageFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
//        //die('1');
//        /**var $product Product */
//        $product = $this->productRepository->getById(1);
//        echo $product->getName();
//        echo $product->getSKU();
//        echo "<br />";
//        echo $product->getPrice();
        return  $resultPage = $this->pageFactory->create();
    }
}