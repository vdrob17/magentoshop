<?php

namespace Ivanko\Notebook\Api;

interface NotebookRepositoryInterface
{
    /**
     * @param integer $id
     * @return \Ivanko\Notebook\Api\Data\NotebookInterface
     */
    public function getById($id);

    /**
     * @param \Ivanko\Notebook\Api\Data\NotebookInterface $notebook
     * @return \Ivanko\Notebook\Api\Data\NotebookInterface
     */
    public function save($notebook);

    /**
     * @param \Ivanko\Notebook\Api\Data\NotebookInterface $notebook
     */
    public function delete($notebook);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList($searchCriteria);

    /**
     * @return integer
     */
    public function getLastPageNumber();
}