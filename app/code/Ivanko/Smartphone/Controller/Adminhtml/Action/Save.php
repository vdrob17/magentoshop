<?php

namespace Ivanko\Smartphone\Controller\Adminhtml\Action;

use Ivanko\Smartphone\Api\Data\SmartphoneInterfaceFactory;
use Ivanko\Smartphone\Api\SmartphoneRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotSaveException;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Smartphone::write";

    /**
     * @var SmartphoneInterfaceFactory
     */
    private $smartphoneFactory;

    /**
     * @var SmartphoneRepositoryInterface
     */
    private $smartphoneRepository;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param SmartphoneInterfaceFactory $smartphoneFactory
     * @param SmartphoneRepositoryInterface $smartphoneRepository
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        Context $context,
        SmartphoneInterfaceFactory $smartphoneFactory,
        SmartphoneRepositoryInterface $smartphoneRepository,
        DataObjectHelper $dataObjectHelper
    )
    {
        parent::__construct($context);
        $this->smartphoneFactory = $smartphoneFactory;
        $this->smartphoneRepository = $smartphoneRepository;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var SmartphoneInterface $smartphone */
        $smartphone = $this->smartphoneFactory->create();
        if (empty($data['id']))
            unset($data['id']);
        $this->dataObjectHelper->populateWithArray($smartphone, $data, SmartphoneInterface::class);
        try {
            $this->smartphoneRepository->save($smartphone);
            $this->messageManager->addSuccessMessage(__("The smartphone was saved success."));
        } catch (CouldNotSaveException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        $this->_redirect("smartphone/action/view");
    }
}
