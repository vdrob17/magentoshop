<?php

namespace JeniaPlah\SecondModule\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class FirstOutput
 * @package JeniaPlah\SecondModule\Block
 */
class SecondOutput extends Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getOutput()
    {
        return __('Output from second module!');
    }
}