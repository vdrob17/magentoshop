<?php

namespace Ivanko\Smartphone\Controller\Adminhtml\Action;

use Magento\Backend\App\Action\Context;
use Ivanko\Smartphone\Api\SmartphoneRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Smartphone::write";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var SmartphoneRepositoryInterface
     */
    private $smartphoneRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param SmartphoneRepositoryInterface $smartphoneRepository
     */
    public function __construct(
        Context $context,
        SmartphoneRepositoryInterface $smartphoneRepository
    )
    {
        parent::__construct($context);
        $this->smartphoneRepository = $smartphoneRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $smartphone = $this->smartphoneRepository->getById($id);
            $this->smartphoneRepository->delete($smartphone);
            $this->messageManager->addSuccessMessage(__("The smartphone was deleted success."));
        } catch (CouldNotDeleteException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        $this->_redirect("smartphone/action/view");
    }
}
