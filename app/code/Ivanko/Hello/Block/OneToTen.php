<?php

namespace Ivanko\Hello\Block;

/**
 * Class HelloWorld
 * @package Ivanko\Hello\Block
 */
class OneToTen extends \Magento\Framework\View\Element\Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHelloText()
    {
        return __("Hello World!");
    }
}