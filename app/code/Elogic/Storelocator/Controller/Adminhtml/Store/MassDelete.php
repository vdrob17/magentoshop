<?php

namespace Elogic\Storelocator\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Elogic\Storelocator\Model\ResourceModel\Store\CollectionFactory;
use Elogic\Storelocator\Api\StoreRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Elogic_Storelocator::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StoreRepositoryInterface $storeRepository
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $store) {
            $this->storeRepository->delete($store);
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        $this->_redirect("storelocator/store/index");
    }
}
