<?php

namespace Ivanko\Notebook\Controller\Adminhtml\Index;

use Ivanko\Notebook\Api\Data\NotebookInterfaceFactory;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Ivanko\Notebook\Api\Data\NotebookInterface;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Notebook::base";

    /**
     * @var NotebookInterfaceFactory
     */
    private $notebookFactory;

    /**
     * @var NotebookRepositoryInterface
     */
    private $notebookRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param NotebookInterfaceFactory $notebookFactory
     * @param NotebookRepositoryInterface $notebookRepository
     */
    public function __construct(
        Context $context,
        NotebookInterfaceFactory $notebookFactory,
        NotebookRepositoryInterface $notebookRepository
    ) {
        parent::__construct($context);
        $this->notebookFactory = $notebookFactory;
        $this->notebookRepository = $notebookRepository;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var NotebookInterface $notebook */
        $notebook = $this->notebookFactory->create();
        if (isset($data['id']))
            $notebook->setId($data['id']);
        $notebook
            ->setName($data['name'])
            ->setProcessor($data['processor'])
            ->setRam($data['ram'])
            ->setVideoMemory($data['video_memory'])
            ->setStatus($data['status']);
        $this->notebookRepository->save($notebook);
        $this->messageManager->addSuccess(__("The notebook was saved success."));
        $this->_redirect("notebooks/index/index");
    }
}
