<?php

namespace Ivanko\Notebook\Api;

interface IvankoAttributeRepositoryInterface
{
    /**
     * @param integer $id
     * @return \Ivanko\Notebook\Api\Data\IvankoAttributeInterface
     */
    public function getById($id);

    /**
     * @param \Ivanko\Notebook\Api\Data\IvankoAttributeInterface $ivankoAttribute
     * @return \Ivanko\Notebook\Api\Data\IvankoAttributeInterface
     */
    public function save($ivankoAttribute);

    /**
     * @param integer $orderId
     * @return \Ivanko\Notebook\Api\Data\IvankoAttributeInterface
     */
    public function getByOrderId($orderId);
}