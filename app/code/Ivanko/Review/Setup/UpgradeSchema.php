<?php

namespace Ivanko\Review\Setup;

use Ivanko\Review\Api\Data\ReviewInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            if ($connection->isTableExists("my_review")) {
                $connection->addColumn(
                    "my_review",
                    ReviewInterface::USER_NAME,
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => "User name"
                    ]
                );
            }
            $installer->endSetup();
        }
        $setup->endSetup();
    }
}