<?php


namespace Elogic\Catalog\Model\Attribute\Source;

class ScreenSize extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('21`'), 'value' => '21'],
                ['label' => __('23`'), 'value' => '23'],
                ['label' => __('24`'), 'value' => '24'],
                ['label' => __('25`'), 'value' => '25'],
                ['label' => __('27`'), 'value' => '27'],
                ['label' => __('28`'), 'value' => '28'],
                ['label' => __('29`'), 'value' => '29'],
            ];
        }
        return $this->_options;
    }
}
