<?php

namespace Ivanko\Notebook\Plugin;

use Ivanko\Notebook\Model\IvankoAttributeRepository;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Ivanko\Notebook\Api\Data\IvankoAttributeInterfaceFactory;
use Ivanko\Notebook\Api\Data\IvankoAttributeInterface;

class Order
{

    /**
     * @var IvankoAttributeRepository
     */
    private $ivankoAttributeRepository;

    /**
     * @var IvankoAttributeInterfaceFactory
     */
    private $attributeInterfaceFactory;

    /**
     * Order constructor.
     * @param IvankoAttributeRepository $ivankoAttributeRepository
     * @param IvankoAttributeInterfaceFactory $attributeInterfaceFactory
     */
    public function __construct(
        IvankoAttributeRepository $ivankoAttributeRepository,
        IvankoAttributeInterfaceFactory $attributeInterfaceFactory
    )
    {
        $this->ivankoAttributeRepository = $ivankoAttributeRepository;
        $this->attributeInterfaceFactory = $attributeInterfaceFactory;
    }

    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $result
     * @return OrderInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function afterSave(
        OrderRepositoryInterface $subject,
        OrderInterface $result
    ) {
        $extensionAttribute = $result->getExtensionAttributes();
        if ($extensionAttribute === null)
            return $result;
        /** @var IvankoAttributeInterface $ivankoAttribute */
//        $ivankoAttribute = $this->attributeInterfaceFactory->create();
//        $ivankoAttribute->setValue('test');
//        $ivankoAttribute->setOrderId($result->getEntityId());
//        $extensionAttribute->setIvankoAttribute($ivankoAttribute);
//        $ivankoAttribute = $extensionAttribute->getIvankoAttribute();
        $this->ivankoAttributeRepository->save($extensionAttribute);
        return $result;
    }

    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $result
     * @return OrderInterface
     */
    public function afterGet(
        OrderRepositoryInterface $subject,
        OrderInterface $result
    ) {
        $ivankoAttribute = $this->ivankoAttributeRepository->getByOrderId($result->getEntityId());
        $result->setExtensionAttributes($ivankoAttribute);
        return $result;
    }
}