<?php

namespace Ivanko\Smartphone\Model\ResourceModel\Smartphone;

use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = SmartphoneInterface::ID;

    public function _construct()
    {
        $this->_init(
            \Ivanko\Smartphone\Model\Smartphone::class,
            \Ivanko\Smartphone\Model\ResourceModel\Smartphone::class
        );
    }
}