<?php

namespace Elogic\Storelocator\Model;

use Elogic\Storelocator\Api\Data;
use Elogic\Storelocator\Api\StoreRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Elogic\Storelocator\Model\ResourceModel\Store as ResourceStore;
use Elogic\Storelocator\Model\StoreFactory as StoreFactory;
use Elogic\Storelocator\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class StoreRepository implements StoreRepositoryInterface
{
    protected $resource;
    protected $storeFactory;
    protected $storeCollectionFactory;
    protected $collectionProcessor;
    protected $searchResultsFactory;

    public function __construct(
        ResourceStore $resource,
        StoreFactory $storeFactory,
        StoreCollectionFactory $storeCollectionFactory,
        Data\StoreSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor = null
    )
    {
        $this->resource = $resource;
        $this->storeFactory = $storeFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }


    /**
     * Save store.
     *
     * @param \Elogic\Storelocator\Api\Data\StoreInterface $store
     * @return \Elogic\Storelocator\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Elogic\Storelocator\Api\Data\StoreInterface $store)
    {
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the page: %1', $exception->getMessage()),
                $exception
            );
        }
        return $store;
    }

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Elogic\Storelocator\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId)
    {
        $store = $this->storeFactory->create();
        $store->load($storeId);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $storeId));
        }
        return $store;
    }

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Elogic\Storelocator\Api\Data\StoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->storeCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\StoreSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete store.
     *
     * @param \Elogic\Storelocator\Api\Data\StoreInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Elogic\Storelocator\Api\Data\StoreInterface $store)
    {
        try {
            $this->resource->delete($store);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the page: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete Stpre by given store Identity
     *
     * @param string $storeId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteById($storeId)
    {
        return $this->delete($this->getById($storeId));
    }
}