<?php

namespace Ivanko\Review\Controller\Index;

use Ivanko\Review\Api\Data\ReviewInterface;
use Ivanko\Review\Api\Data\ReviewInterfaceFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Ivanko\Review\Api\ReviewRepositoryInterface;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var ReviewRepositoryInterface
     */
    private $repositoryInterface;

    /**
     * @var ReviewInterfaceFactory
     */
    private $reviewFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ReviewRepositoryInterface $repositoryInterface
     * @param ReviewInterfaceFactory $reviewFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ReviewRepositoryInterface $repositoryInterface,
        ReviewInterfaceFactory $reviewFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->repositoryInterface = $repositoryInterface;
        $this->reviewFactory = $reviewFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var ReviewInterface $review */
        $review = $this->reviewFactory->create();
        $review
            ->setTitle('100 title')
            ->setContent('100 content')
            ->setUserName('100 user');
        $this->repositoryInterface->save($review);
        return $this->pageFactory->create();
    }
}
