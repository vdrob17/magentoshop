<?php

namespace Ivanko\Smartphone\Model;

use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Magento\Framework\Model\AbstractModel;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone as Resource;

class Smartphone extends AbstractModel implements SmartphoneInterface
{
    protected function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param int $id
     * @return SmartphoneInterface|AbstractModel
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);
        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->getData(self::MODEL);
    }

    /**
     * @param string $model
     * @return SmartphoneInterface
     */
    public function setModel($model)
    {
        $this->setData(self::MODEL, $model);
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplay()
    {
        return $this->getData(self::DISPLAY);
    }

    /**
     * @param string $display
     * @return SmartphoneInterface
     */
    public function setDisplay($display)
    {
        $this->setData(self::DISPLAY, $display);
        return $this;
    }

    /**
     * @return string
     */
    public function getResolution()
    {
        return $this->getData(self::RESOLUTION);
    }

    /**
     * @param string $resolution
     * @return SmartphoneInterface
     */
    public function setResolution($resolution)
    {
        $this->setData(self::RESOLUTION, $resolution);
        return $this;
    }

    /**
     * @return string
     */
    public function getProcessor()
    {
        return $this->getData(self::PROCESSOR);
    }

    /**
     * @param string $processor
     * @return SmartphoneInterface
     */
    public function setProcessor($processor)
    {
        $this->setData(self::PROCESSOR, $processor);
        return $this;
    }

    /**
     * @return string
     */
    public function getOs()
    {
        return $this->getData(self::OS);
    }

    /**
     * @param string $os
     * @return SmartphoneInterface
     */
    public function setOs($os)
    {
        $this->setData(self::OS, $os);
        return $this;
    }

    /**
     * @return integer
     */
    public function getStorage()
    {
        return $this->getData(self::STORAGE);
    }

    /**
     * @param integer $storage
     * @return SmartphoneInterface
     */
    public function setStorage($storage)
    {
        $this->setData(self::STORAGE, $storage);
        return $this;
    }

    /**
     * @return integer
     */
    public function getRam()
    {
        return $this->getData(self::RAM);
    }

    /**
     * @param integer $ram
     * @return SmartphoneInterface
     */
    public function setRam($ram)
    {
        $this->setData(self::RAM, $ram);
        return $this;
    }

    /**
     * @return string
     */
    public function getCamera()
    {
        return $this->getData(self::CAMERA);
    }

    /**
     * @param string $camera
     * @return SmartphoneInterface
     */
    public function setCamera($camera)
    {
        $this->setData(self::CAMERA, $camera);
        return $this;
    }

    /**
     * @return integer
     */
    public function getBattery()
    {
        return $this->getData(self::BATTERY);
    }

    /**
     * @param integer $battery
     * @return SmartphoneInterface
     */
    public function setBattery($battery)
    {
        $this->setData(self::BATTERY, $battery);
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->getData(self::PRICE);
    }

    /**
     * @param float $price
     * @return SmartphoneInterface
     */
    public function setPrice($price)
    {
        $this->setData(self::PRICE, $price);
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return SmartphoneInterface
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param integer $status
     * @return SmartphoneInterface
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }
}
