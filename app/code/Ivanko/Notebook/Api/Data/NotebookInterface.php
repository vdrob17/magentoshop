<?php

namespace Ivanko\Notebook\Api\Data;

interface NotebookInterface
{
    const ID = 'id';
    const STATUS = 'status';
    const NAME = 'name';
    const PROCESSOR = 'processor';
    const RAM = 'ram';
    const VIDEO_MEMORY = 'video_memory';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param integer $id
     * @return NotebookInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return NotebookInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getProcessor();

    /**
     * @param string $processor
     * @return NotebookInterface
     */
    public function setProcessor($processor);

    /**
     * @return integer
     */
    public function getRam();

    /**
     * @param integer $ram
     * @return NotebookInterface
     */
    public function setRam($ram);

    /**
     * @return integer
     */
    public function getVideoMemory();

    /**
     * @param integer $video_memory
     * @return NotebookInterface
     */
    public function setVideoMemory($video_memory);

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param integer $status
     * @return NotebookInterface
     */
    public function setStatus($status);
}