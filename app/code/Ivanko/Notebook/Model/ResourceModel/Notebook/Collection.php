<?php

namespace Ivanko\Notebook\Model\ResourceModel\Notebook;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = NotebookInterface::ID;

    public function _construct()
    {
        $this->_init(
            \Ivanko\Notebook\Model\Notebook::class,
            \Ivanko\Notebook\Model\ResourceModel\Notebook::class
        );
    }
}