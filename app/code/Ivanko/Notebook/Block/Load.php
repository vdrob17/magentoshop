<?php

namespace Ivanko\Notebook\Block;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;
use Ivanko\Notebook\Model\ResourceModel\Notebook as Resource;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResultsInterface;

class Load extends Template
{
    /**
     * @var NotebookInterface $notebook
     */
    private $notebook;

    /**
     * @var SearchResultsInterface $notebookCollection
     */
    private $notebookCollection;

    /**
     * @var NotebookRepositoryInterface
     */
    private $notebookRepository;

    /**
     * @var Resource\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * Load constructor.
     * @param Template\Context $context
     * @param NotebookRepositoryInterface $notebookRepository
     * @param Resource\CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Template\Context $context,
        NotebookRepositoryInterface $notebookRepository,
        Resource\CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        parent::__construct($context);
        $this->notebookRepository = $notebookRepository;
        $this->collectionFactory = $collectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return NotebookInterface
     */
    public function getNotebook()
    {
        $this->notebook = $this->notebookRepository->getById($this->getRequest()->getParam('id'));
        return $this->notebook;
    }

    /**
     * @return Resource\Collection|array
     */
    public function getNotebookCollection()
    {
//        $this->notebookCollection = $this->collectionFactory->create();
//        $this->notebookCollection->setPageSize(8);
//        $this->notebookCollection->setCurPage($this->getRequest()->getParam('page'));
//
////        /** Homework (shows last five notebooks from database) */
////        return array_slice($this->notebookCollection->getItems(), -5, 5);

        $searchCriteria = $this->searchCriteriaBuilder
            ->setPageSize(8)
            ->setCurrentPage($this->getRequest()->getParam('page'))
            ->create();
        $this->notebookCollection = $this->notebookRepository->getList($searchCriteria);
        return $this->notebookCollection->getItems();
    }

    /**
     * @return integer
     */
    public function getNumberOfPages()
    {
        return $this->notebookRepository->getLastPageNumber();
    }
}