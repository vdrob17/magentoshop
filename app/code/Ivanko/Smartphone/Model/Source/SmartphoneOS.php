<?php

namespace Ivanko\Smartphone\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class SmartphoneOS implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'Android', 'label' => 'Android'],
            ['value' => 'iOS', 'label' => 'iOS']
        ];
    }
}