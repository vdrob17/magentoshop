<?php

namespace Ivanko\Smartphone\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Ivanko\Smartphone\Model\Smartphone as SmartphoneModel;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone\Collection;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone\CollectionFactory;

class SmartphoneDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData(): array
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var SmartphoneModel $smartphone */
        foreach ($items as $smartphone) {
            $this->loadedData[$smartphone->getId()] = $smartphone->getData();
        }

        $data = $this->dataPersistor->get('smartphone');
        if (!empty($data)) {
            $smartphone = $this->collection->getNewEmptyItem();
            $smartphone->setData($data);
            $this->loadedData[$smartphone->getId()] = $smartphone->getData();
            $this->dataPersistor->clear('smartphone');
        }

        return $this->loadedData;
    }
}