<?php

namespace Ivanko\Review\Model;

use Ivanko\Review\Api\Data\ReviewInterface;
use Ivanko\Review\Api\Data\ReviewInterfaceFactory;
use Ivanko\Review\Api\ReviewRepositoryInterface;
use Ivanko\Review\Model\ResourceModel\Review as Resource;

class ReviewRepository implements ReviewRepositoryInterface
{
    /**
     * @var ReviewInterfaceFactory
     */
    private $reviewFactory;

    /**
     * @var Resource
     */
    private $reviewResource;

    /**
     * ReviewRepository constructor.
     * @param ReviewInterfaceFactory $reviewFactory
     * @param Resource $reviewResource
     */
    public function __construct(
        ReviewInterfaceFactory $reviewFactory,
        Resource $reviewResource
    )
    {
        $this->reviewFactory = $reviewFactory;
        $this->reviewResource = $reviewResource;
    }

    /**
     * @param integer $id
     * @return ReviewInterface
     */
    public function getById($id)
    {
        /** @var ReviewInterface $review */
        $currReview = $this->reviewFactory->create();
        $this->reviewResource->load($currReview, $id);
        return $currReview;
    }

    /**
     * @param ReviewInterface $review
     * @return ReviewRepositoryInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save($review)
    {
        $this->reviewResource->save($review);
        return $this;
    }

    /**
     * @param ReviewInterface $review
     * @return void
     * @throws \Exception
     */
    public function delete($review)
    {
        $this->reviewResource->delete($review);
    }
}