<?php

namespace Ivanko\Review\Model\ResourceModel;

use Ivanko\Review\Api\Data\ReviewInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Review extends AbstractDb
{
    const TABLE_NAME = 'my_review';

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ReviewInterface::ID);
    }
}
