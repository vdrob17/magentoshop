<?php

namespace Elogic\Storelocator\Model\ResourceModel\Store;

use Elogic\Storelocator\Api\Data\StoreInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = StoreInterface::ENTITY_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Storelocator\Model\Store::class, \Elogic\Storelocator\Model\ResourceModel\Store::class);
    }
}
