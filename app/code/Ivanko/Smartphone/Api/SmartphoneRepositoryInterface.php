<?php

namespace Ivanko\Smartphone\Api;

interface SmartphoneRepositoryInterface
{
    /**
     * @param integer $id
     * @return \Ivanko\Smartphone\Api\Data\SmartphoneInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param \Ivanko\Smartphone\Api\Data\SmartphoneInterface $notebook
     * @return \Ivanko\Smartphone\Api\Data\SmartphoneInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save($notebook);

    /**
     * @param \Ivanko\Smartphone\Api\Data\SmartphoneInterface $notebook
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete($notebook);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList($searchCriteria);
}