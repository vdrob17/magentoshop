<?php

namespace Ivanko\Notebook\Controller\Action;

use Ivanko\Notebook\Api\NotebookRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;

class Delete extends Action
{
    /**
     * @var NotebookRepositoryInterface
     */
    private $notebookRepository;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Index constructor.
     * @param Context $context
     * @param NotebookRepositoryInterface $notebookRepository
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        NotebookRepositoryInterface $notebookRepository,
        ManagerInterface $messageManager
    )
    {
        parent::__construct($context);
        $this->notebookRepository = $notebookRepository;
        $this->messageManager = $messageManager;
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var NotebookInterface $notebook */
        $notebook = $this->notebookRepository->getById($this->getRequest()->getParam('id'));
        $this->notebookRepository->delete($notebook);
        $this->messageManager->addSuccess(__("The notebook was deleted success."));
        $this->_redirect("notebook/action/index");
    }
}
