<?php

namespace Ivanko\Review\Api\Data;

interface ReviewInterface
{
    const ID = 'id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const USER_NAME = 'user_name';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param integer $id
     * @return ReviewInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return ReviewInterface
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     * @return ReviewInterface
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getUserName();

    /**
     * @param $user_name
     * @return ReviewInterface
     */
    public function setUserName($user_name);
}
