<?php

namespace Ivanko\Notebook\Controller\Action;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Ivanko\Notebook\Api\Data\NotebookInterfaceFactory;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Save extends Action
{
    /**
     * @var NotebookInterfaceFactory
     */
    private $notebookFactory;

    private $notebookRepository;

    /**
     * Index constructor.
     * @param Context $context
     * @param NotebookInterfaceFactory $notebookFactory
     * @param NotebookRepositoryInterface $notebookRepository
     */
    public function __construct(
        Context $context,
        NotebookInterfaceFactory $notebookFactory,
        NotebookRepositoryInterface $notebookRepository
    )
    {
        parent::__construct($context);
        $this->notebookFactory = $notebookFactory;
        $this->notebookRepository = $notebookRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Exception
     */
    public function execute()
    {
        /** @var NotebookInterface $notebook */
        $notebook = $this->notebookFactory->create();
        if ($this->getRequest()->getParam('id') != null) {
            $notebook->setId($this->getRequest()->getParam('id'));
        }
        $notebook
            ->setName($this->getRequest()->getParam('name'))
            ->setProcessor($this->getRequest()->getParam('processor'))
            ->setRam($this->getRequest()->getParam('ram'))
            ->setVideoMemory($this->getRequest()->getParam('video_memory'));
        $this->notebookRepository->save($notebook);
        $this->_redirect("notebook/action/view/id", ['id' => $notebook->getId()]);
    }
}
