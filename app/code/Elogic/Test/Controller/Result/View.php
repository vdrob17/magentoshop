<?php

namespace Elogic\Test\Controller\Result;

use Magento\Cms\Block\Page;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageResultFactory;

    /**
     * View constructor.
     * @param PageFactory $pageFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $pageFactory,
        Context $context
    )
    {
        $this->pageResultFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->pageResultFactory->create();
    }
}
