<?php

namespace Elogic\Events\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;


class StorelocatorLoadAfter  implements ObserverInterface
{
    /**
     * Apply model save operation
     *
     * @param Observer $observer
     * @throws \Magento\Framework\Validator\Exception
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Elogic\Storelocator\Api\Data\StoreInterface $entity */
        $entity = $observer->getEvent()->getObject();
        $entity->setTitle($entity->getTitle()." observerd");
    }
}