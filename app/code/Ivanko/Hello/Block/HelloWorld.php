<?php

namespace Ivanko\Hello\Block;

/**
 * Class HelloWorld
 * @package Ivanko\Hello\Block
 */
class HelloWorld extends \Magento\Framework\View\Element\Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHelloText()
    {
        return __("Hello World!!!");
    }
}